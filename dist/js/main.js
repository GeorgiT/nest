$(document).ready(function () {
    // Fixed menu opening and closinq start
    $('.open-fix-menu').on('click', function () {
        $('.fixed-menu').css({
            'right': 0
        });
        $('.opened-fixed-menu').addClass('d-block');
        $('.closer-container .nav-link').addClass('rotation');
    });
    $('.closer-container .nav-link').on('click', function () {
        $(this).removeClass('rotation');
        $('.opened-fixed-menu').removeClass('d-block');
        $('.fixed-menu').css({
            'right': '-100%'
        });
    });
    // Fixed menu opening and closinq end
    // Main page owl carousel creation start
    $('.owl-carousel-main').owlCarousel({
        loop: true,
        responsiveClass: true,
        margin: 15,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2,
                nav: false,
                loop: true,
                dots: false,
                stagePadding: 60,
                autoplay: true
            },
            600: {
                items: 5,
                nav: false,
                loop: true,
                dots: false,
                autoplay: true
            },
            1000: {
                items: 10,
                nav: false,
                loop: true,
                dots: false,
                autoplay: true
            }
        },
    });
    // Main page owl carousel creation end
    // Main page projects text fade in start
    split = function (element) {
        words = $(element).text().split('');
        for (i in words) {
            words[i] = '<span>' + words[i] + '</span>';
        }
        text = words.join('');
        $(element).html(text);
    };

    textify = function(element,method,delay) {
        split(element);
        $(element + ' span').css('opacity','0')
        $(element + ' span').css('position','relative');
        in_speed = 10;
        count = 0;
        setTimeout(function(){
            count = 0;
            $(element + ' span').each(function () {
                if(method == 'fade'){
                    $(this).delay(0 + in_speed * count).animate({ opacity: '1' }, 400);
                } else if(method == 'bounce'){
                    $(this).delay(0 + in_speed * count).animate({ opacity: '1','top':'-4px'}, 220,'easeOutCubic');
                    $(this).delay(0 + in_speed * count/4).animate({ opacity: '1','top':'0px'}, 220);
                }
                count++;
            });
        },delay);
    };

    var image_box = $('.project-boxes');
    $(image_box[0]).addClass('hover');

    $('.project-boxes').hover(
        function() {
            let data = $(this).attr('data-image');
            let images = $('.header-part-background video');
            $(image_box).removeClass('hover');
            $(this).addClass('hover');
            $(this).find('.text-fade-in').addClass('show');
            $(this).find('.link-fade-in').addClass('show');
            $(this).find('.scroll').hide();
            if(images.length == 0){
            }else{
                $(images).removeClass('active-bg');
                $(images[data]).addClass('active-bg');
            }
            // textify('.show.text-fade-in','fade', 100);
            // textify('.show.link-fade-in','fade', 100);
        }, function() {
            $(this).find('.text-fade-in').removeClass('show');
            $(this).find('.link-fade-in').removeClass('show');
            $(this).find('.scroll').show();
        }
    );
    // Main page projects text fade in end
    // Maps open an close effect start\
    $('.left-side-controller').on('click', function(){
        let data = $(this).attr('data-id');
        switch(data) {
            case '1':
                $('.left-side').toggleClass('sizing-half sizing-full');
                $('.right-side').addClass('to-right');
                $(this).find('img').attr('src', 'uploads/minus.png');
                $(this).prev().addClass('hide');
                setTimeout(function () {
                    $('.main-map').toggleClass('d-none');
                },200)
                $(this).attr('data-id', '2');
                break;
            case '2':
                $('.main-map').toggleClass('d-none');
                $('.left-side').toggleClass('sizing-full sizing-half');
                $('.right-side').removeClass('to-right');
                $(this).find('img').attr('src', 'uploads/plus.png');
                $(this).prev().removeClass('hide');
                $(this).attr('data-id', '1');
                break;
            default:
                $(this).attr('data-id', '1');
        }
    });

    $('.right-side-controller').on('click', function(){
        let data = $(this).attr('data-id');
        switch(data) {
            case '1':
                $('.right-side').toggleClass('sizing-half sizing-full');
                $('.left-side').addClass('to-left');
                $(this).find('img').attr('src', 'uploads/minus.png');
                $(this).prev().addClass('hide');
                setTimeout(function () {
                    $('.main-map').toggleClass('d-none');
                },200)
                $(this).attr('data-id', '2');
                break;
            case '2':
                $('.main-map').toggleClass('d-none');
                $('.right-side').toggleClass('sizing-full sizing-half');
                $('.left-side').removeClass('to-left');
                $(this).find('img').attr('src', 'uploads/plus.png');
                $(this).prev().removeClass('hide');
                $(this).attr('data-id', '1');
                break;
            default:
                $(this).attr('data-id', '1');
        }
    });
    // Maps open an close effect end\
    $('.owl-carousel-projects').owlCarousel({
        stagePadding: 200,
        loop:true,
        margin:10,
        nav:false,
        items:1,
        lazyLoad: true,
        nav:true,
        responsive:{
            0:{
                items:1,
                stagePadding: 60
            },
            600:{
                items:1,
                stagePadding: 100
            },
            1000:{
                items:1,
                stagePadding: 200
            },
            1200:{
                items:1,
                stagePadding: 250
            },
            1400:{
                items:1,
                stagePadding: 300
            },
            1600:{
                items:1,
                stagePadding: 350
            },
            1800:{
                items:1,
                stagePadding: 400
            }
        }
    });
    // Map hover start
    $('.opener').hover(
        function() {
            $(this).prev().addClass('hovered');
        }, function() {
            $(this).prev().removeClass('hovered');
        }
    );
    // Map hover end
    // ScrollLine start
    ScrollLine($('.scroll'));
    // ScrollLine end

    // Scroll hide show in a product page start
    var header = $('.product-scroll');
    var range = 200;

    $(window).on('scroll', function () {
        var scrollTop = $(this).scrollTop(),
            height = header.outerHeight(),
            offset = height / 2,
            calc = 1 - (scrollTop - offset + range) / range;

        if (calc > '0.0674') {
            header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
            header.css({ 'opacity': 0 });
        }
    });
    // Scroll hide show in a product page end
})

// Scroll line height
function ScrollLine(line) {
    let mainBox = $(line).parent().parent().parent();
    let positionLine = parseInt($(mainBox).height()) - 130;
    $(line).css({
        'top': positionLine,
    });
}

    // Other maps
window.addEventListener("load", function(event) {
    if(document.getElementById('map-1') != null){
        var uluru = {lat: 55.753492, lng: 37.633999};
        map1 = new google.maps.Map(document.getElementById('map-1'), {
            zoom: 14,
            disableDefaultUI: true,
            scrollwheel: false,
            center: uluru,
            styles: [
                {
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                }
            ]
        });

        var markers = [
            [55.762546, 37.645248, 'Hotel Manson']
        ];

        var hotels = [
            {
                title: 'Hotel Manson',
                address: 'ул. Таганская, 20'
            }
        ];

        var icon = {
            url: "uploads/map-marker.png", // url
            scaledSize: new google.maps.Size(50, 50), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

        for (var i = 0; i < markers.length; i++) {
            var mark = markers[i];
            var marker = new google.maps.Marker({
                position: {lat: mark[0], lng: mark[1]},
                map: map1,
                icon: icon,
                title: mark[2]
            });
            marker.addListener('mouseover', function(e) {
                let title = $(e.xa.target).parent().attr('title');
                $(e.xa.target).parent().addClass('window-content show');
                let select = hotels.filter(hotel => hotel.title == title);
                if(title == select[0].title){
                    $(e.xa.target).parent().append(`<div class="marker-info-window">${select[0].title}<br>${select[0].address}</div>`);
                }
            });
            marker.addListener('mouseout', function(e) {
                $(e.xa.target).parent().removeClass('show');
                let lastChildren = $(e.xa.target).parent().children().last();
                $(lastChildren).remove();
            });
        }
    }
    if(document.getElementById('map-2') != null){
        var uluru = {lat: 55.753492, lng: 37.633999};
        var map = new google.maps.Map(document.getElementById('map-2'), {
            zoom: 14,
            disableDefaultUI: true,
            scrollwheel: false,
            center: uluru,
            styles: [
                {
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#0a0b0f"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#21242b"
                        }
                    ]
                }
            ]
        });

        var markers = [
            [55.762546, 37.645248, 'Hotel Manson']
        ];

        var hotels = [
            {
                title: 'Hotel Manson',
                address: 'ул. Таганская, 20'
            }
        ];

        var icon = {
            url: "uploads/map-marker.png", // url
            scaledSize: new google.maps.Size(50, 50), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

        for (var i = 0; i < markers.length; i++) {
            var mark = markers[i];
            var marker = new google.maps.Marker({
                position: {lat: mark[0], lng: mark[1]},
                map: map,
                icon: icon,
                title: mark[2]
            });
            marker.addListener('mouseover', function(e) {
                let title = $(e.xa.target).parent().attr('title');
                $(e.xa.target).parent().addClass('window-content show');
                let select = hotels.filter(hotel => hotel.title == title);
                if(title == select[0].title){
                    $(e.xa.target).parent().append(`<div class="marker-info-window">${select[0].title}<br>${select[0].address}</div>`);
                }
            });
            marker.addListener('mouseout', function(e) {
                $(e.xa.target).parent().removeClass('show');
                let lastChildren = $(e.xa.target).parent().children().last();
                $(lastChildren).remove();
            });
        }
    }
});