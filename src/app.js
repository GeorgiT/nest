// Import JS and CSS components and libraries
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

// libraries
import 'bootstrap';
import 'popper.js';
// custom css
import './scss/app.scss';